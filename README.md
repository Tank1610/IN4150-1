# IN4150-1
Repo for Lab 1 of Distributed Algorithms

## Nature of the exercise
The program implements the Birman-Schiper-Stephenson algorithm for causal ordering of broadcast messages with Java/RMI. It can be demonstrated to run on multiple machines, by adjusting the IPs in the source code (instead of putting "localhost").

## Structure 
There is three parts :
- a client side where N client threads are creating packets at a regular pace, each packet being represented by a new thread and having a random delay,
- a broker that records all the transmitted packets, listens to the requests from the clients and pass them to the servers,
- a server side where N servers are listening to the broker, getting the packages, and treating them (commit them or buffer them).

In order to run the program, you must run the different parts in that order :
- first, run the servers,
- then, run the broker that connects to the servers,
- finally, run the clients that connect to the broker.
