package com.company;

import java.util.concurrent.CountDownLatch;

public class Main {
    public static void main(String[] argv) throws InterruptedException
    // the throws InterruptedException is needed for the countdown latch...
    {
        int N = 5;
        Thread[] P = new Thread[N];


        CountDownLatch startSignal = new CountDownLatch(1);
        CountDownLatch doneSignal = new CountDownLatch(N);

        for(int i = 0; i < N; i++)
        {
            P[i] = new Thread(new Process(i, N, startSignal, doneSignal));
            P[i].start();
        }
        // wait for all the threads to
        doneSignal.await();
        startSignal.countDown();

    }



}
