package com.company;

import com.sun.org.apache.xpath.internal.operations.Bool;

import java.lang.reflect.Array;
import java.rmi.RemoteException;
import java.util.ArrayList;

/**
 * @author Alain Defrance
 */
public class AddImplementation implements AddInterface {

    private ArrayList local_clk;
    private ArrayList<ArrayList> buffer;
    private ArrayList<Integer> buffer_id;
    int id, N; // personal ID of the process. Number of processes.


    public AddImplementation(ArrayList remote_clk,int remote_id, int remote_N){
        // problem , the reference is copied instead of the values.
        local_clk = new ArrayList();
        buffer = new ArrayList<ArrayList>();
        buffer_id = new ArrayList<Integer>();
        N = remote_N;
        id = remote_id;

        for (int i = 0; i < N; i++) {
            local_clk.add(remote_clk.get(i));
        }

    }

    public ArrayList increaseClock()
    {
        local_clk.set(id, (int) local_clk.get(id) + 1);
        return local_clk;
    }

    public int receive(ArrayList t, int sender_id) throws RemoteException {


        System.out.print("ID "+id+" : receives "+t+"from ID " + sender_id +"\n");

        // CHECK YOUR STUFF
        System.out.print("ID "+id+" : checks timestamp "+t+" with clock " + local_clk+"\n");

        boolean isKnowingMore = true;
        for (int i = 0; i < N; i++)
        {
            // checks if all the components are superior or equal to the local clock
            // the case where i == sender_id is seen after.
            if(i != sender_id)
                isKnowingMore = isKnowingMore & ((int) local_clk.get(i) >= (int) t.get(i));
        }

        if ((int) local_clk.get(sender_id) == (int) t.get(sender_id) - 1 && isKnowingMore) // expected packet
        {
            System.out.println("ID "+id+" : packet from "+sender_id+" was expected. Updating clock.");
            local_clk.set(sender_id, (int) t.get(sender_id));
            // Check the Buffer !
            if (!buffer.isEmpty())
            {
                // copy the last element in a local variable
                ArrayList t2 = new ArrayList();
                for (int i = 0; i < N; i++) {
                    t2.add(buffer.get(buffer.size() - 1).get(i));
                }
                // pop out the last element
                buffer.remove(buffer.size()-1);

                // pop out the last element in buffer_id too
                int id2 = buffer_id.get(buffer_id.size()-1);
                buffer_id.remove(buffer_id.size()-1);

                // re-launch that function with this new element !
                receive(t2, id2);
            }

        } else {
            System.out.println("ID "+id+" : packet from "+sender_id+" was not expected. Discarded. (Later, buffer ?)");
            buffer.add(t);
            buffer_id.add(sender_id);
        }



        return 0;
    }
}