package com.company;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.CountDownLatch;

/**
 * @author Alain Defrance
 */
public class Process implements Runnable {

    private int id, N;
    private ArrayList<Integer> clk;
    private final CountDownLatch startSignal;
    private final CountDownLatch doneSignal;

    public Process(int remote_id, int remote_N, CountDownLatch remote_startSignal, CountDownLatch remote_doneSignal)
    {
        this.id = remote_id;
        this.N = remote_N;
        this.startSignal = remote_startSignal;
        this.doneSignal = remote_doneSignal;
        clk = new ArrayList<Integer>();
        for (int i = 0; i < N; i++)
        {
            this.clk.add(i, 0); // vector clock to 0 at the beginning.
        }
    }
    /*
    public synchronized void server() {

        //System.out.println("Starting Server...");
        try {
            // 10000 est le port sur lequel sera publié le service. Nous devons le préciser à la fois sur le registry et à la fois à la création du stub.
            AddInterface skeleton = (AddInterface) UnicastRemoteObject.exportObject(new AddImplementation(clk, id, N), 10000 + id); // Génère un stub vers notre service.
            Registry registry = LocateRegistry.createRegistry(10000+id);
            registry.rebind("Receive", skeleton); // publie notre instance sous le nom "Add"

            System.out.println("Server "+ this.id +" started.");
            doneSignal.countDown();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    */

    public void main_process() {
        try {


            System.out.println("Starting client "+ this.id +"...");
            Registry[] regs = new Registry[N];

            AddInterface[] stub = new AddInterface[N];

            for(int i = 0; i < N; i ++) {
                regs[i] = LocateRegistry.getRegistry(10000+i);
                stub[i] = (AddInterface) java.rmi.Naming.lookup("rmi://192.168.0.21:" + (10000 + i) + "/Receive");
            }

            while(true)
            {
                // broadcast timestamp
                Thread.sleep(100*(10*id+1));

                // broadcast timestamp
                clk = stub[id].increaseClock();

                for(int i = 0; i < N; i ++)
                {
                    if(i != id)
                        stub[i].receive(clk, id);
                    // when a process receives a timestamp, it will do the necessary with it.
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        main_process();
    }
}