package com.company;

import java.util.ArrayList;

public class RecordedPacket {
    /*
    I don't want to do getters and setters,
    public variables are fine for what I want.
     */

    public ArrayList<Integer> timestamp;
    public int sender_id;

    public RecordedPacket(ArrayList<Integer> t, int sid)
    {
        sender_id = sid;
        timestamp = new ArrayList<Integer>();
        for (Integer val : t)
            timestamp.add(val);

    }

}
