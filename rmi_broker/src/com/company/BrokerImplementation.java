package com.company;

import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;

public class BrokerImplementation implements BrokerInterface {

    private int N;
    private ArrayList<RecordedPacket> Records;

    private static AddInterface[] stub;

    public BrokerImplementation(int N) throws RemoteException, MalformedURLException, NotBoundException {
        this.N = N;
        Records = new ArrayList<RecordedPacket>();

        // Create one registry for each process (even though unly the registries for foreign processes are used.
        Registry[] regs = new Registry[N];
        stub = new AddInterface[N];
        for(int i = 0; i < N; i ++) {
            regs[i] = LocateRegistry.getRegistry(10000+i);
            stub[i] = (AddInterface) java.rmi.Naming.lookup("rmi://localhost:" + (10000 + i) + "/Receive");
            System.out.println("Connection established with server "+i);
        }

    }

    public int brokerReceive(ArrayList<Integer> t, int sender_id, int receiver_id, int remote_tries) throws RemoteException {
        // "BEHAVES LIKE A CLIENT" : serves the message and record it too.
        RecordedPacket r;
        r = new RecordedPacket(t, sender_id);
        Records.add(r);
        stub[receiver_id].receive(t, sender_id, remote_tries);

        return 0;
    }

    public ArrayList brokerIncreaseClock(int id) {
        // connect
        ArrayList<Integer> res = new ArrayList<Integer>();
        try {
            res = stub[id].increaseClock();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return res;
    }
}
