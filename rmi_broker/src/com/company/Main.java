package com.company;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;

public class Main {


    private static Registry registry;

    public static void main(String[] args) {
	// write your code here

        int N = 5;

        try {
            BrokerInterface skeleton = (BrokerInterface) UnicastRemoteObject.exportObject(new BrokerImplementation(N), 20000); // Génère un stub vers notre service.

            registry = LocateRegistry.createRegistry(20000);
            registry.rebind("BrokerReceive", skeleton); // publie notre instance sous le nom "Add"

            System.out.println("Broker started on port 20000.");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
