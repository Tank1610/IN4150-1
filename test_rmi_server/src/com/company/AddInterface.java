package com.company;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

/**
 * @author Alain Defrance
 */
public interface AddInterface extends Remote {
    public int receive(ArrayList t, int sender_id) throws RemoteException;
    public ArrayList increaseClock() throws  RemoteException;
}