package com.company;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

/**
 * @author Alain Defrance
 */
public interface AddInterface extends Remote {
    public int receive(ArrayList t, int sender_id, int remote_tries) throws RemoteException;
    public ArrayList<Integer> increaseClock() throws  RemoteException;
}