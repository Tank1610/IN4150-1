package com.company;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

public interface BrokerInterface extends Remote {
    public int brokerReceive(ArrayList t, int sender_id, int receiver_id, int remote_tries) throws RemoteException;
    public ArrayList brokerIncreaseClock(int id) throws RemoteException;
}
