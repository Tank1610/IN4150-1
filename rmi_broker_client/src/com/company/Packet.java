package com.company;

import com.sun.corba.se.pept.broker.Broker;

import java.rmi.RemoteException;
import java.util.ArrayList;

public class Packet implements Runnable {

    private BrokerInterface stub;
    private ArrayList<Integer> timestamp;
    private int id, receiver_id;

    // Constructor
    public Packet(BrokerInterface remote_stub, ArrayList<Integer> remote_clk, int remote_id, int remote_receiver_id){
        id = remote_id;
        receiver_id = remote_receiver_id;
        stub = remote_stub;
        timestamp = new ArrayList<Integer>();
        for (Integer integer : remote_clk) {
            timestamp.add(integer);
        }
    }

    @Override
    public void run() {
        // The try-catch blocks are mandatory for IntelliJ IDEA to compile. I guess they are safer anyways...
        try {
            try {
                // random wait (simulate travel time)
                Thread.sleep((long)(1000*(Math.random())));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            /* remote_tries = -1 :
                this special value means it's an actual packet sent by the network.
                this remote_tries variable is meant to go through the buffer when receive is called recursively.
             */
            stub.brokerReceive(timestamp, id, receiver_id,-1);

            //stub.receive(timestamp, id, rec_id, -1);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}
