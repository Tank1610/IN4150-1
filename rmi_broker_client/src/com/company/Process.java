package com.company;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;

/**
 * @author Alain Defrance
 */
public class Process implements Runnable {

    private int id, N;
    private ArrayList<Integer> clk;
    private final CountDownLatch startSignal;
    private final CountDownLatch doneSignal;

    private static Registry registry;

    public Process(int remote_id, int remote_N, CountDownLatch remote_startSignal, CountDownLatch remote_doneSignal)
    {
        this.id = remote_id;
        this.N = remote_N;
        this.startSignal = remote_startSignal;
        this.doneSignal = remote_doneSignal;
        clk = new ArrayList<Integer>();
        for (int i = 0; i < N; i++)
        {
            this.clk.add(i, 0); // vector clock to 0 at the beginning.
        }
    }

    public synchronized void server() {

        try {
            AddInterface skeleton = (AddInterface) UnicastRemoteObject.exportObject(new AddImplementation(clk, id, N), 10000 + id); // Génère un stub vers notre service.

            registry = LocateRegistry.createRegistry(10000+id);
            registry.rebind("Receive", skeleton); // publie notre instance sous le nom "Add"

            System.out.println("Server "+ this.id +" started.");

            // This signals the main thread that this server started.
            // It's a countdown latch because the main thread needs ALL servers to be started to launch the clients.
            doneSignal.countDown();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void main_process() {
        try {
            // server();
            // This startSignal is a lock, it is unlocked by the Main thread when all servers have started
            // startSignal.await();

            System.out.println("Starting client "+ this.id +"...");

            // Create one registry for each process (even though unly the registries for foreign processes are used.

            // connection to broker
             Registry regs = LocateRegistry.getRegistry(20000);
             BrokerInterface stub = (BrokerInterface) java.rmi.Naming.lookup("rmi://localhost:" + (20000) + "/BrokerReceive");


            while(true)
            {
                // Sleep before two sends
                Thread.sleep(100*(id*10+1));

                // broadcast timestamp : first, increment clock, then, create packets to be sent.
                clk = stub.brokerIncreaseClock(id);
                for(int i = 0; i < N; i ++)
                {
                    if(i != id)
                    {
                        /* broadcast to everyone except itself.
                        I create a dedicated thread for each packet. Each packet will have a random wait.
                        This allows me to simulate random travel time.
                        */
                        Thread packet = new Thread(new Packet(stub, clk, id, i));
                        packet.start();
                    }
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        main_process();
    }
}