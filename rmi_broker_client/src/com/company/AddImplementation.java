package com.company;

import com.sun.org.apache.xpath.internal.operations.Bool;

import java.lang.reflect.Array;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import static java.lang.Thread.sleep;

/**
 * @author Alain Defrance
 */
public class AddImplementation implements AddInterface {

    private ArrayList local_clk;
    private ArrayList<ArrayList> buffer;
    private ArrayList<Integer> buffer_id;
    int id, N; // personal ID of the process. Number of processes.


    public AddImplementation(ArrayList remote_clk,int remote_id, int remote_N){
        // problem , the reference is copied instead of the values.
        local_clk = new ArrayList();
        buffer = new ArrayList<ArrayList>();
        buffer_id = new ArrayList<Integer>();
        N = remote_N;
        id = remote_id;

        for (int i = 0; i < N; i++) {
            local_clk.add(remote_clk.get(i));
        }

    }

    public ArrayList increaseClock()
    {
        local_clk.set(id, (int) local_clk.get(id) + 1);
        return local_clk;
    }


    public int receive(ArrayList t, int sender_id, int remote_tries) throws RemoteException {

        System.out.print("ID "+id+" : checks packet from "+ sender_id+" with timestamp "+t+" with clock " + local_clk+"\n");
        int tries = remote_tries;

        boolean isKnowingMore = true;
        for (int i = 0; i < N; i++)
        {
            // checks if all the components are superior or equal to the local clock (part of the Algo)
            // the thing is, if any of the comparison (local_clk >= timestamp) is not true,
            // then isKnowingMore is set to false (with boolean &)
            // ... the case where i == sender_id is seen after.
            if(i != sender_id)
                isKnowingMore = isKnowingMore & ((int) local_clk.get(i) >= (int) t.get(i));
        }

        if ((int) local_clk.get(sender_id) == (int) t.get(sender_id) - 1 && isKnowingMore) // expected packet
        {
            System.out.println("ID "+id+" : packet from "+sender_id+" was expected. Updating clock.");
            local_clk.set(sender_id, (int) t.get(sender_id));
            tries=0; // This means : now check ALL packets in the buffer.
        } else {
            System.out.println("ID "+id+" : packet from "+sender_id+" was not expected. Buffered.");
            buffer.add(t);
            buffer_id.add(sender_id);

            // tries == -1 means "actually received packet, not from buffer
            if (tries == -1)
                tries = buffer.size(); // This means : if the network packet is buffered, no need to check the buffer at all, the local_clk didn't change after all !
        }

        // Check the Buffer !
        if (tries <= buffer.size() - 1)
        {
            // copy the last unchecked element in a local variable
            ArrayList t2 = new ArrayList<Integer>();
            for (int i = 0; i < N; i++) {
                t2.add(buffer.get(buffer.size() - 1 - tries).get(i));
            }
            // pop out the last unchecked element
            buffer.remove(buffer.size()-1 - tries);

            // pop out the last unchecked element in buffer_id too
            int id2 = buffer_id.get(buffer_id.size()-1 - tries);
            buffer_id.remove(buffer_id.size()-1 - tries);

            // re-launch that function with this new element ! go forward in the buffer with tries+1
            receive(t2, id2, tries+1);
        }




        return 0;
    }
}